<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::get('/', [HomeController::class, 'index']);

Route::get('/home', [HomeController::class, 'redirect'])->middleware('auth', 'verified');

Route::post('/rdv', [HomeController::class, 'rdv_traitement']);

Route::get('/mon_rdv', [HomeController::class, 'mon_rdv_traitement']);

Route::get('/modifer_rdv/{id}', [HomeController::class, 'modifier_rdv_traitement']);

Route::get('/supprimer_rdv/{id}', [HomeController::class, 'supprimer_rdv_traitement']);

Route::middleware(['auth:sanctum','verified',])->get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

Route::get('/ajouter_docteur', [AdminController::class, 'ajouter_docteur_traitement']);

Route::post('/enregistrer_docteur', [AdminController::class, 'enregistrer_docteur_traitement']);

Route::get('/afficher_rdv', [AdminController::class, 'afficher_rdv_traitement']);

Route::get('/accepter_rdv/{id}', [AdminController::class, 'accepter_rdv_traitement']);

Route::get('/annuler_rdv/{id}', [AdminController::class, 'annuler_rdv_traitement']);

Route::get('/afficher_docteur', [AdminController::class, 'afficher_docteur_traitement']);

Route::get('/modifier_docteur/{id}', [AdminController::class, 'modifier_docteur_traitement']);

Route::get('/supprimer_docteur/{id}', [AdminController::class, 'supprimer_docteur_traitement']);

Route::post('/editer_docteur/{id}', [AdminController::class, 'editer_docteur_traitement']);

Route::get('/envoyer_message/{id}', [AdminController::class, 'envoyer_message_traitement']);

Route::post('/envoyer_email/{id}', [AdminController::class, 'envoyer_email_traitement']);


























