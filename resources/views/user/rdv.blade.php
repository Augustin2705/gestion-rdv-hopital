<div class="page-section">
    <div class="container">
      <h1 class="text-center wow fadeInUp">Prendre Rendez-vous</h1>

      <form class="main-form" action="{{url('rdv')}}" method="POST">
        @csrf
        <div class="row mt-5 ">
          <div class="col-12 col-sm-6 py-2 wow fadeInLeft">
            <input name="nom"  type="text" class="form-control" placeholder="Nom complet">
          </div>
          <div class="col-12 col-sm-6 py-2 wow fadeInRight">
            <input name="email"  type="email" class="form-control" placeholder="Adresse  e-mail">
          </div>
          <div class="col-12 col-sm-6 py-2 wow fadeInLeft" data-wow-delay="300ms">
            <input name="date" type="date" class="form-control">
          </div>
          <div class="col-12 col-sm-6 py-2 wow fadeInRight" data-wow-delay="300ms">
            <select  name="docteur_id"  class="custom-select">

              <option>---Choisir un docteur---</option>
              @foreach($docteurs as $docteur)

              <option value="{{$docteur->id}}" >{{$docteur->nom}} ({{$docteur->specialite}})</option>

              @endforeach

            </select>
          </div>
          <div class="col-12 py-2 wow fadeInUp" data-wow-delay="300ms">
            <input name="telephone" type="text" class="form-control" placeholder="Numéro">
          </div>
          <div class="col-12 py-2 wow fadeInUp" data-wow-delay="300ms">
            <textarea name="message" id="message" class="form-control" rows="6" placeholder="Entrez votre message"></textarea>
          </div>
        </div>

        <button type="submit" class="btn btn-primary mt-3 wow zoomIn">Envoyer</button>
      </form>
    </div>
  </div> 