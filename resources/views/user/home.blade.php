<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    
    <link rel="stylesheet" href="../assets/css/maicons.css">

    <link rel="stylesheet" href="../assets/css/bootstrap.css">

    <link rel="stylesheet" href="../assets/vendor/owl-carousel/css/owl.carousel.css">

    <link rel="stylesheet" href="../assets/vendor/animate/animate.css">

    <link rel="stylesheet" href="../assets/css/theme.css">
    <title>Hospital</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-light navbar-light">
    <div class="container">
        <ul class="navbar-nav me-auto">
            <li class="nav-item">
                <a class="nav-link active" href="{{url('/')}}">Hospital</a>
            </li>
        </ul>



        @if(Route::has('login'))


        @auth
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="{{url('mon_rdv')}}" class="nav-link" style="color:black">Mon RDV</a>
            </li>
        </ul>


        <x-app-layout>
        </x-app-layout>

        @else
        <ul class="navbar-nav ml-auto">
            <li class="nav-item mr-auto">
                <a class="nav-link" href="{{route('login')}}">Connexion</a>
            </li>

            <li class="d-flex">
                <a class="nav-link " href="{{route('register')}}">Inscription</a>
            </li>
        </ul>

        @endauth

        @endif


    </div>
    </nav>

    <div class="container">
        @if (session('success'))
        
            <div class="alert alert-success alert-dismissible fade show" role="alert" id="alertSuccess">
                {{ session('success') }}
            </div>

            <script>
                // JavaScript pour masquer l'alerte de succès après 5 secondes (5000 ms)
                setTimeout(function() {
                    document.getElementById('alertSuccess').style.display = 'none';
                }, 5000);
            </script>

        @endif
    </div>



    @include('user.docteur')

    @include('user.rdv')


   

    
</body>
</html>