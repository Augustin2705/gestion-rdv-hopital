<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <meta name="copyright" content="MACode ID, https://macodeid.com/">


  <link rel="stylesheet" href="../assets/css/maicons.css">

  <link rel="stylesheet" href="../assets/css/bootstrap.css">

  <link rel="stylesheet" href="../assets/vendor/owl-carousel/css/owl.carousel.css">

  <link rel="stylesheet" href="../assets/vendor/animate/animate.css">

  <link rel="stylesheet" href="../assets/css/theme.css">
</head>
<body>

 

  <div class="page-section">
    <div class="container">
      <h1 class="text-center mb-5 wow fadeInUp">Nos Médecins</h1>

      <div class="owl-carousel wow fadeInUp" id="doctorSlideshow">

         @foreach($docteurs as $docteur)

        <div class="item">
          <div class="card-doctor">
            <div class="header">
              <img src="../storage/{{$docteur->image}}" alt="{{$docteur->nom}}">
              <div class="meta">
                <a href="#"><span class="mai-call"></span></a>
                <a href="#"><span class="mai-logo-whatsapp"></span></a>
              </div>
            </div>
            <div class="body">
              <p class="text-xl mb-0">{{$docteur->nom}}</p>
              <span class="text-sm text-grey">{{ucfirst($docteur->specialite)}}</span>
            </div>
          </div>
        </div>

        @endforeach

      </div>
    </div>
  </div>

 

<script src="../assets/js/jquery-3.5.1.min.js"></script>

<script src="../assets/js/bootstrap.bundle.min.js"></script>

<script src="../assets/vendor/owl-carousel/js/owl.carousel.min.js"></script>

<script src="../assets/vendor/wow/wow.min.js"></script>

<script src="../assets/js/theme.js"></script>
  
</body>
</html>