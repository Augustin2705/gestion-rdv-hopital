<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    
    <link rel="stylesheet" href="../assets/css/maicons.css">

    <link rel="stylesheet" href="../assets/css/bootstrap.css">

    <link rel="stylesheet" href="../assets/vendor/owl-carousel/css/owl.carousel.css">

    <link rel="stylesheet" href="../assets/vendor/animate/animate.css">

    <link rel="stylesheet" href="../assets/css/theme.css">
    <title>Hospital</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-light navbar-light">
    <div class="container">
        <ul class="navbar-nav me-auto">
            <li class="nav-item">
                <a class="nav-link active" href="{{url('/')}}">Hospital</a>
            </li>
        </ul>



        @if(Route::has('login'))


        @auth
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a href="{{url('mon_rdv')}}" class="nav-link" style="color:black">Mon RDV</a>
            </li>
        </ul>


        <x-app-layout>
        </x-app-layout>

        @else
        <ul class="navbar-nav ml-auto">
            <li class="nav-item mr-auto">
                <a class="nav-link" href="{{route('login')}}">Connexion</a>
            </li>

            <li class="d-flex">
                <a class="nav-link " href="{{route('register')}}">Inscription</a>
            </li>
        </ul>

        @endauth

        @endif


    </div>
    </nav>

    <div class="container">
    @if ($rdvs->isEmpty())
        <p>Aucun rendez-vous disponible pour le moment.</p>
    @else
    <div class="row justify-content-center">
        @foreach ($rdvs as $rdv)
        <div class="col-lg-6 mb-4">
            <div class="card ">
                <div class="card-header bg-dark text-white">
                    {{ \Carbon\Carbon::parse($rdv->date)->translatedFormat('l j F Y') }}
                </div>
                
                <a href="" class=" d-flex align-items-center text-decoration-none position-relative">
                <div class="card-body wow fadeInUp">
                    <table class="table">
                        <tr>
                            <td>
                                <div class="card-content">
                                    <img src="../storage/{{$rdv->docteur->image}}" class=" w-20" alt="{{ $rdv->docteur_id }}">
                                </div>
                            </td>

                            <td>
                                <p class="card-title">{{ $rdv->docteur->nom }}</p>
                                <p class=" text-muted">{{ucfirst ($rdv->docteur->specialite) }}</p>
                            </td>

                            <td class="text-end">
                                <svg class="w-6 h-6 text-gray-800 dark:text-dark" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m9 5 7 7-7 7"/>
                                </svg>
                            </td>

                           
                        </tr>
                    </table>
                    <div class="position-absolute top-0 start-0 w-100 h-100 bg-white opacity-0 transition opacity-25 hover:opacity-100"></div>
                </a>

                <script>
    function hoverEffect(element) {
        let hoverDiv = element.querySelector('.bg-white');
        hoverDiv.classList.toggle('opacity-25');
        hoverDiv.classList.toggle('opacity-100');
    }
</script>

                
 

                    
                   
                  


                    <p class="card-text">Message: {{ $rdv->message }}</p>
                    <p class="card-text">Status: {{ $rdv->status }}</p>
                </div>
                <div class="card-footer">
                    <div class="float-right">
                        <a href="{{ url('modifier_rdv', $rdv->id) }}" class="btn btn-info">{{ __('Modifier') }}</a>
                        <a href="{{ url('supprimer_rdv', $rdv->id) }}" class="btn btn-danger" onclick="return confirm('{{ __('Voulez-vous vraiment supprimer?') }}')">{{ __('Supprimer') }}</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>





  


    <div class="container text-center ">
        <table class="table">
            <tr  >
                <th>Nom du docteur</th>
                <th>Date</th>
                <th>Message</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            @if ($rdvs->isEmpty())
        <p>Aucun rendez-vous disponible pour le moment.</p>
    @else
            @foreach ($rdvs as $rdv)

            <tr>
                

                <td>{{$rdv->docteur_id}}</td>
                <td>
                    @php
                        $date = \Carbon\Carbon::parse($rdv->date)->locale('fr_FR');
                    @endphp
                    {{ $date->translatedFormat('l j F Y') }}
                </td>
                <td>{{$rdv->message}}</td>
                <td>{{$rdv->status}}</td>
                <td>
                    <a href="{{url('modifier_rdv', $rdv->id)}}" class="btn btn-info">Modifier</a>
                    <a href="{{url('supprimer_rdv', $rdv->id)}}" class="btn btn-danger" onclick="return confirm('Voulez-vous vraiment supprimer?')">Supprimer</a>
                </td>


              
            </tr>
             @endforeach
           
            @endif
           
        </table>
    </div>


    <script src="../assets/js/jquery-3.5.1.min.js"></script>

<script src="../assets/js/bootstrap.bundle.min.js"></script>

<script src="../assets/vendor/owl-carousel/js/owl.carousel.min.js"></script>

<script src="../assets/vendor/wow/wow.min.js"></script>

<script src="../assets/js/theme.js"></script>



  


   

    
</body>
</html>