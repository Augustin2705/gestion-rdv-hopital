
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Corona Admin</title>
    <!-- plugins:css -->
    @include('admin.ccs')
  </head>
  <body>
    <div class="container-scroller">
      <div class="row p-0 m-0 proBanner" id="proBanner">
        <div class="col-md-12 p-0 m-0">
          <div class="card-body card-body-padding d-flex align-items-center justify-content-between">
            <div class="ps-lg-1">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 font-weight-medium me-3 buy-now-text">Free 24/7 customer support, updates, and more with this template!</p>
                <a href="https://www.bootstrapdash.com/product/corona-free/?utm_source=organic&utm_medium=banner&utm_campaign=buynow_demo" target="_blank" class="btn me-2 buy-now-btn border-0">Get Pro</a>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
              <a href="https://www.bootstrapdash.com/product/corona-free/"><i class="mdi mdi-home me-3 text-white"></i></a>
              <button id="bannerClose" class="btn border-0 p-0">
                <i class="mdi mdi-close text-white me-0"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- partial:partials/_sidebar.html -->
       @include('admin.sidebar')
      <!-- partial -->
        @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
        <div class="container text-center">
        <table class="table">
            <tr>
                <th>Nom du docteur</th>
                <th>Téléphone</th>
                <th>Spécialité</th>
                <th>Chambre</th>
                <th>image</th>
                <th>Action</th>
            </tr>

            @foreach ($docteurs as $docteur)
             
            <tr>
                <td>{{$docteur->nom}}</td>
                <td>{{$docteur->telephone}}</td>
                <td>{{$docteur->specialite}}</td>
                <td>{{$docteur->chambre}}</td>
                <td><img src="../storage/{{$docteur->image}}" alt="{{$docteur->nom}}"></td>
                <td>
                    <a href="{{url('modifier_docteur', $docteur->id)}}" class="btn btn-primary">Modifier</a>
                    <a href="{{url('supprimer_docteur',$docteur->id)}}" class="btn btn-danger" onclick="return confirm('Voulez-vous vraiment supprimer?')">Supprimer</a>
                </td>
            </tr>
            @endforeach


        </table>
    </div>

    <!-- container-scroller -->
    <!-- plugins:js -->
      @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>