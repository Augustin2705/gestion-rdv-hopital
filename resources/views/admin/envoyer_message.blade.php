
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
     <style type="text/css">
     

         label{
            display: inline-block;
            width: 200px;
         }

     </style>
    <base  href="/public">

    <!-- plugins:css -->
    @include('admin.ccs')
  </head>
  <body>
    <div class="container-scroller">
      <div class="row p-0 m-0 proBanner" id="proBanner">
        <div class="col-md-12 p-0 m-0">
          <div class="card-body card-body-padding d-flex align-items-center justify-content-between">
            <div class="ps-lg-1">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 font-weight-medium me-3 buy-now-text">Free 24/7 customer support, updates, and more with this template!</p>
                <a href="https://www.bootstrapdash.com/product/corona-free/?utm_source=organic&utm_medium=banner&utm_campaign=buynow_demo" target="_blank" class="btn me-2 buy-now-btn border-0">Get Pro</a>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
              <a href="https://www.bootstrapdash.com/product/corona-free/"><i class="mdi mdi-home me-3 text-white"></i></a>
              <button id="bannerClose" class="btn border-0 p-0">
                <i class="mdi mdi-close text-white me-0"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- partial:partials/_sidebar.html -->
       @include('admin.sidebar')
      <!-- partial -->
        @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">



            <div class="container" align="center" style= "padding-top :100px">
            
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert" id="alertSuccess">
                    {{ session('success') }}
                </div>
                <script>
                  // JavaScript pour masquer l'alerte de succès après 5 secondes (5000 ms)
                  setTimeout(function() {
                      document.getElementById('alertSuccess').style.display = 'none';
                  }, 5000);
                </script>
            @endif

                <form action="{{url('/envoyer_email', $rdv->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div style="padding:15px">
                        <label for="salutation">Salutation  :</label>
                        <input type="text" id="nom" name="salutation" style="color:black">
                    </div>

                    <div style="padding:15px">
                        <label for="corps">Corps:  </label>
                        <input type="text" id="telephone" name="corps" style="color:black">
                    </div>

                 

                    <div style="padding:15px">
                        <label for="texteaction">Texte Action:  </label>
                        <input type="text" id="chambre" name="texteAction" style="color:black">
                    </div>

                    <div style="padding:15px">
                        <label for="actionurl">Action URL:  </label>
                        <input type="text" id="chambre" name="actionURL" style="color:black">
                    </div>

                    <div style="padding:15px">
                        <label for="Fin">Fin :  </label>
                        <input type="text" id="chambre" name="fin" style="color:black">
                    </div>



                 
                    
                    <div style="padding:15px">
                        <input type="submit" class="btn btn-success">
                    </div>



                </form>
            </div>
       </div>
    <!-- container-scroller -->

        
    <!-- plugins:js -->
      @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>