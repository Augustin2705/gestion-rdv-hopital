
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Corona Admin</title>
    <!-- plugins:css -->
    @include('admin.ccs')
  </head>
  <body>
    <div class="container-scroller">
      <div class="row p-0 m-0 proBanner" id="proBanner">
        <div class="col-md-12 p-0 m-0">
          <div class="card-body card-body-padding d-flex align-items-center justify-content-between">
            <div class="ps-lg-1">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 font-weight-medium me-3 buy-now-text">Free 24/7 customer support, updates, and more with this template!</p>
                <a href="https://www.bootstrapdash.com/product/corona-free/?utm_source=organic&utm_medium=banner&utm_campaign=buynow_demo" target="_blank" class="btn me-2 buy-now-btn border-0">Get Pro</a>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
              <a href="https://www.bootstrapdash.com/product/corona-free/"><i class="mdi mdi-home me-3 text-white"></i></a>
              <button id="bannerClose" class="btn border-0 p-0">
                <i class="mdi mdi-close text-white me-0"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- partial:partials/_sidebar.html -->
       @include('admin.sidebar')
      <!-- partial -->
        @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
        <div class="container text-center">
        <table class="table">
            <tr>
                <th>Nom du patient</th>
                <th>Email</th>
                <th>Téléphone</th>
                <th>Docteur</th>
                <th>Date</th>
                <th>Message</th>
                <th>Statut</th>
                <th>Action</th>
                <th>Envoyer message</th>
            </tr>

            @foreach ($rdvs as $rdv)
            
            <tr>
                <td>{{$rdv->nom}}</td>
                <td>{{$rdv->email}}</td>
                <td>{{$rdv->telephone}}</td>
                <td>{{$rdv->docteur}}</td>
                <td>{{$rdv->date}}</td>
                <td>{{$rdv->message}}</td>
                <td>{{$rdv->status}}</td>
                <td>
                    <a href="{{url('accepter_rdv',$rdv->id)}}" class="btn btn-success">Accepter</a>
                    <a href="{{url('annuler_rdv',$rdv->id)}}" class="btn btn-danger" onclick="return confirm('Voulez-vous vraiment annuler?')">Annuler</a>
                </td>

                <td>
                    <a href="{{url('envoyer_message',$rdv->id)}}" class="btn btn-primary">Envoyer mail</a>
                </td>


            </tr>

            @endforeach

        </table>
    </div>

        </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
      @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>