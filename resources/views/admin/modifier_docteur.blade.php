
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
     <style type="text/css">

         label{
            display: inline-block;
            width: 200px;
         }

     </style>
     <base href="/public">
    <!-- plugins:css -->
    @include('admin.ccs')
  </head>
  <body>
    <div class="container-scroller">
      <div class="row p-0 m-0 proBanner" id="proBanner">
        <div class="col-md-12 p-0 m-0">
          <div class="card-body card-body-padding d-flex align-items-center justify-content-between">
            <div class="ps-lg-1">
              <div class="d-flex align-items-center justify-content-between">
                <p class="mb-0 font-weight-medium me-3 buy-now-text">Free 24/7 customer support, updates, and more with this template!</p>
                <a href="https://www.bootstrapdash.com/product/corona-free/?utm_source=organic&utm_medium=banner&utm_campaign=buynow_demo" target="_blank" class="btn me-2 buy-now-btn border-0">Get Pro</a>
              </div>
            </div>
            <div class="d-flex align-items-center justify-content-between">
              <a href="https://www.bootstrapdash.com/product/corona-free/"><i class="mdi mdi-home me-3 text-white"></i></a>
              <button id="bannerClose" class="btn border-0 p-0">
                <i class="mdi mdi-close text-white me-0"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- partial:partials/_sidebar.html -->
       @include('admin.sidebar')
      <!-- partial -->
        @include('admin.navbar')
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">



            <div class="container" align="center" style= "padding-top :100px">
            
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert" id="alertSuccess">
                    {{ session('success') }}
                </div>
                <script>
                  // JavaScript pour masquer l'alerte de succès après 5 secondes (5000 ms)
                  setTimeout(function() {
                      document.getElementById('alertSuccess').style.display = 'none';
                  }, 5000);
                </script>
            @endif

                <form action="{{url('/editer_docteur', $docteur->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div style="padding:15px">
                        <label for="nom">Nom  :</label>
                        <input type="text" id="nom" name="nom" style="color:black" value="{{$docteur->nom}}">
                    </div>

                    <div style="padding:15px">
                        <label for="telephone">Télephone :  </label>
                        <input type="number" id="telephone" name="telephone" style="color:black" value="{{$docteur->telephone}}">
                    </div>

                    <div style="padding:15px">
                        <label for="nom">Spécialité :</label>
                        <select name="specialite" id="" style="color:black">
                            <option value="{{$docteur->specialite}}">{{$docteur->specialite}}</option>
                            <option value="anesthesiste">Anesthésiste</option>
                            <option value="cardiologue">Cardiologue</option>
                            <option value="genecologue">Génecologue</option>
                            <option value="generaliste">Généraliste</option>
                            <option value="ophtalmologue">Ophtalmologue</option>
                        </select>
                    </div>

                    <div style="padding:15px">
                        <label for="chambre">Numero de chambre  </label>
                        <input type="text" id="chambre" name="chambre" style="color:black" value="{{$docteur->chambre }}">
                    </div>

                    <div style="padding:15px">
                        <label for="nom">Ancienne Image  </label>
                        <img style="width:150px; height:150px;object-fit:cover" src="../storage/{{$docteur->image}}" alt="{{$docteur->nom}}"><br>

                        <label for="nom">Changer Image  </label>
                        <input type="file"  name="fichier" >
                    </div>

                    
                    <div style="padding:15px">
                        <input type="submit" class="btn btn-success">
                    </div>



                </form>
            </div>
       </div>
    <!-- container-scroller -->

        
    <!-- plugins:js -->
      @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>