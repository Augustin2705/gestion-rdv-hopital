<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Docteurs;
use App\Models\rdv;
use App\Notifications\MaNotification;
use Notification;

class AdminController extends Controller
{
    public function ajouter_docteur_traitement(){

        if(Auth::id()){

            if(Auth::user()->type_utilisateur == '1'){

                return view('admin.ajouter_docteur');
            }
            else{
                return redirect()->back();
            }   
            

        }
        else{
            return redirect('login');
        }


    }

    public function enregistrer_docteur_traitement(Request $request){
        $request->validate([
            'nom' => 'required|string',
            'telephone' => 'required|string',
            'specialite' => 'required|string',
            'chambre' => 'required|string',
            'fichier' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048', // Taille max : 2 Mo
        ]);

        $docteur = new Docteurs();
        $docteur->nom = $request->nom;
        $docteur->telephone = $request->telephone;
        $docteur->specialite = $request->specialite;
        $docteur->chambre = $request->chambre;

        $image = $request->file('fichier');
        $nom_image = time() . '.' . $image->getClientOriginalExtension();
        
        // Stocker l'image dans le dossier "public/storage/doctors"
        $image->storeAs('public/doctors', $nom_image);

        // Enregistrer le chemin de l'image dans la base de données
        $docteur->image = 'doctors/' . $nom_image;

        $docteur->save();

        return redirect()->back()->with('success', 'Docteur ajouté avec succès');
    }

    public function afficher_rdv_traitement(){


        if (Auth::id()) {

            if(Auth::user()->type_utilisateur == '1'){

                $rdvs = rdv::all();
                return view('admin.afficher_rdv', compact('rdvs'));
            }
            else{
                return redirect()->back();
            }   
            
        }else{
            return redirect('login');
        }
 

    }

    public function accepter_rdv_traitement($id){

        $rdv = rdv::find($id);
        $rdv->status = "Accepté";
        $rdv->save();

        return redirect()->back()->with('success', 'Rendez-vous accepté avec succès');
    }

    public function annuler_rdv_traitement($id){

        $rdv = rdv::find($id);
        $rdv->status = "Annulé";
        $rdv->save();

        return redirect()->back()->with('success', 'Rendez-vous annulé avec succès');
    }

    public function afficher_docteur_traitement(){

        $docteurs = Docteurs::all();
        return view('admin.afficher_docteur', compact('docteurs'));

    }

    public function modifier_docteur_traitement($id){

        $docteur=Docteurs::find($id);
        return view('admin.modifier_docteur', compact('docteur') );

    }

    public function supprimer_docteur_traitement($id){

        $docteur = Docteurs::find($id);
        $docteur->delete();
        return redirect()->back()->with('success', 'Docteur supprimé avec succès');
    }

    public function editer_docteur_traitement(Request $request, $id){

        $docteur = Docteurs::find($id);
        $docteur->nom = $request->nom;
        $docteur->telephone = $request->telephone;
        $docteur->specialite = $request->specialite;
        $docteur->chambre = $request->chambre;

        $image = $request->fichier;

        if($image){

            $nom_image = time() . '.' . $image->getClientOriginalExtension();

            $request->fichier->move('public/doctors', $nom_image);
        
            // Stocker l'image dans le dossier "public/storage/doctors"
            
    
            // Enregistrer le chemin de l'image dans la base de données
            $docteur->image = $nom_image;

        }


        $docteur->save();
        return redirect()->back()->with('success', 'Docteur modifié avec succès'); 
    }

    public function envoyer_message_traitement($id){

        $rdv = rdv::find($id);
        return view('admin.envoyer_message', compact('rdv'));

    }

    public function envoyer_email_traitement(Request $request, $id){

        $rdv = rdv::find($id);

        $details=[
            'salutation' => $request->salutation,
            'corps' => $request->corps,
            'texteAction' => $request->texteAction,
            'actionURL' => $request->actionURL,
            'fin' => $request->fin
        ];

        Notification::send($rdv, new MaNotification($details));
        return redirect()->back()->with('success', 'Email envoyé avec succès');  
    }
}