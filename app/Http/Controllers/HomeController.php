<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Docteurs;
use App\Models\rdv;

class HomeController extends Controller
{
    public function redirect()
    {
        if (Auth::id()) {

            if(Auth::user()->type_utilisateur == 0){

                $docteurs = docteurs::all();
                return view('user.home', compact('docteurs'));
            }
            else{
                return view('admin.home');
            }   
        }
       else {
            return redirect()->back()->with('error', 'Vous devez vous connecter pour accéder à cette page');
        }
    }

    public function index()
    {
        if(Auth::id()){
            return redirect()->route('home');
        }
        else{
            $docteurs = docteurs::all();
            return view('user.home', compact('docteurs'));
        }
      
    }

    public function rdv_traitement(Request $request){

        $rdv = new rdv();

        $rdv->nom=$request->nom;
        $rdv->telephone=$request->telephone;
        $rdv->email=$request->email;
        $rdv->message=$request->message;
        $rdv->date=$request->date;
        $rdv->status="En attente";

        if(Auth::id()){

            $rdv->user_id=Auth()->id();

        }
        $docteurId = $request->input('docteur_id');
        $rdv->docteur_id = $docteurId;
        
        $rdv->save();
      
        return redirect()->back()->with('success', 'Rendez-vous enregistré avec succès');
    }

    public function mon_rdv_traitement(){

        if(Auth::id()){

            if(Auth::user()->type_utilisateur == 0){
          
                $user_id = Auth::id();
                $rdvs = rdv::where('user_id', $user_id)->with('docteur')->get();
                return view('user.mon_rdv', compact('rdvs'));
            }
            }
            else{
                return redirect()->back();
            }   
 
    }

    public function supprimer_rdv_traitement($id){

        $rdvs = rdv::find($id);
        $rdvs->delete();

        return redirect()->back()->with('success', 'Rendez-vous annulé avec succès');
    }
}
