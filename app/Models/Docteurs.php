<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasMany;


class Docteurs extends Model
{
    use HasFactory;

    
    public function rdv()
    {
        return $this->hasMany(rdv::class);
    }
}
