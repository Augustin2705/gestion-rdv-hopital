<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class rdv extends Model
{
    use HasFactory;

    public function docteur()
    {
        return $this->belongsTo(Docteurs::class, 'docteur_id');
    }
}
